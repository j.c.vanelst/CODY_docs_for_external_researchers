# CODY documentation for external researchers

CODY is the friendly name for a subversion repository with MATLAB code (The Mathworks, TM), made for [YOUth](youthonderzoek.nl). It contains many EEG, Eye Tracking, computer tasks experiments for the 'waves' in this long term cohort study.

With the current project, we start on the documentation an external researcher would need to work with the output from CODY, i.e., the data and all info needed to process the data and get an answer to research questions. This means mostly descriptions of data structures, headers, column and row values, in short, all info needed to work with data as can be given out by the Data Manager. 

## Structuring guidelines
This repository is organised in different folders for each experiment type. Within that type, different files (or folders with files) may reside for different actual experiments. Also, some descriptions are *generic* (eg. Eye Tracking gaze data always has the same structure) and others are specific for an experiment. Please keep in mind when extending documentation, that documentation can be given out so that it is *complete* yet not overly *redundant*. 

## Markdown
All documentation is written in [Markdown](https://en.wikipedia.org/wiki/Markdown). As of yet, the documentation is limited to the Eye Tracking experiments. 

## Programming language neutrality
The documentation currently cannot be 100% language independent (as of yet). All data is saved in Matlab data stuctures (.mat extension). This data is the 'real source' data, as saved to our research storage. In the future, perhaps other (human readable) formats could be exported, based upon these files. However, this data raw can also be processed using GNU Octave (which has identical vector indexing and types as Matlab), or Python, R or other languages can be used to work with .mat files. However, the latter options would imply sightly different descriptions of how the data will look or would need to be indexed. 

## Media
Be sure **not to add** any **images, pdf's, docx (or any other binary text processing format), movies or audio** in this repository. Only markdown (or other source code, in plain text) should be added here. You may [link](https://upload.wikimedia.org/wikipedia/commons/d/d1/220_Hz_sine_wave.ogg) to media, but may not add it tot this codebase. 

Jacco van Elst created this repository on 20170125.
Roy Hessels will use this repository as the basis for the automatic data-analysis and quality controls for the eye-tracking domain.