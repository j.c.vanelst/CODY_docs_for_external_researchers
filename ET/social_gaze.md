# Social Gaze experiments
In this experiment, a central face stimulus is animated to cue with the eyes (direct to averted gaze). In the next phase, a peripheral target appears, either to the left or right side, and either congruent or incongruent.

**Important:** Due to a programming error in the social gaze experiments (discovered april 2019), the cue direction and congruency information in the matlab files is incorrect!! The current texture correctly specifies the face used in the trial (see table with image names below). Use this image to derive cue direction information. The target direction information in the matlab files is correct. This can then be combined with the cue direction from the image name to derive congruency of a trial. As a result, cue direction, target direction and congruency are NOT independent from face identity!

## Data file tags
5 and 10 month social gaze experiments are tagged *infsgaze*  
9 year experiments are tagged *chsgaze*  

## Conditions
About conditions:
10 Faces (5 male, 5 female)
2 Directions (target left or right)
2 Gaze cues (congruent and incongruent)


## Trial info
At the trial level (saved to disk after each trial ), There are *three* types of .mat files saved to disk:

type of trial file  | example name (trial 1)                                     | estimated (median) file size
--------------------|------------------------------------------------------------|------------------------------
gaze data           | B12345_5m_infsgaze_20180115_1703_gazedata_1.mat            |  200 KB (op to 500 KB in case of bad data quality)
onset data          | B12345_5m_infsgaze_20180115_1703_onsets_upto_trial_1.mat   |  1 KB (± 1000 bytes)
trial info data     | B12345_5m_infsgaze_20180115_1703_trial_1.mat               |  1 KB (± 1000 bytes)

Similar, aggregated over trials versions of two of these three types can be found if the experiment was completely finished or when a 'clean quit' was used. The onset info is never aggregated as a separate file. We'd strongly recommend to use the trial based file info throughout your analyses, if possible, since they are the real unprocessed 'source' data.

The infant trials have longer presentation phases for the 'eye cue' and 'reward', so in theory, the child versions of the gazedata mat-files could even be smaller, However, data quality in combination with inattention and bearing in mind that this is a gaze contingent task, the actual file sizes may sometimes be quite larger than this estimation (500 KB), which hardly happens in 'child' waves. The infant versions have 40 trials, the child version 80 trials.


### Detailed trial info

column | Name            | Description                                | Data shape & type | codings/extra
-------|-----------------|--------------------------------------------|-------------------|------------------
1      | task_type       | Code for the wave 0 for around 0, 9 for around 9  (etc)| Double (0, 3, 9) | Can be used to double check the wave the experiment belongs to
2      | cur_tex         | Unique numbers from 1-40 (around 0) or 1-80 (around 9) | Double (1-nTrials) | Numbers are used as an index to a fixed 'list' (cell struct).
3      | cur_LR          | Wether the gaze cue was to the left (2) or the right (1)  | Double (1 or 2)| 
4      | cur_IC          | Wether the gaze cue was incongruent (1) or congruent (2)  | Double (1 or 2)| 
5      | cur_target      | Which of the five possible target stimuli was presented (1-5) | Double (1, 2, 3 4, or 5) | Coding: 1:bird, 2:cow, 3:pink flower, 4:red flower, 5: spiral 
6      | cur_target_LR   | Wether the peripheral stimulus was presented to the left (2) or the right (1)  | Double (1 or 2) | 
7      | gazed_cent      | Wether the center AOI received 20 milliseconds worth of eye data | Boolean (0 or 1) |  
8      | gazed_peri      | Wether the peripheral AOI received 20 milliseconds worth of eye data | Boolean (0 or 1) |  
9      | gazed_both      | Wether the peripheral and center AOI received 20 milliseconds worth of eye data during the trial | Boolean (0 or 1)
10     | onset_fix       | Timestamp for the fixation cross onset| Float/Long int timestamp eg, 1.376684754280000e+02  | long int
11     | onset_face1     | Timestamp for the direct gaze frame | Float/Long int timestamp eg, 1.376684754280000e+02  |  
12     | onset_face2     | Timestamp for the averted gaze frame| Float/Long int timestamp eg, 1.376684754280000e+02  |  
13     | onset_reward    | Timestamp for the 'reward' (spinning) onset| Float/Long int timestamp eg, 1.376684754280000e+02  | 
14     | offset_reward   | Timestamp for the end of the trial (end of reward) | Float/Long int timestamp eg, 1.376684754280000e+02 |  | 

### Finding out which face image was used from the data
All images are preloaded into memory (see socialgaze.m), There are the 40 'first frame' (direct gaze) images and 40 'second frame' images (averted gaze) in the randomisation vector. In the *around 9* variant, a vector with **two times** the unique numbers 1-40 (80 trials) are shuffled in place, In the *around 0*  variant, there are only 40 trials, so not repetitions. This index vector was used to select from this exact 'list' (struct) during the experiment (see below). So, if in the data you find cur_tex value 3, the item administered as direct was 'face01ULI_01.bmp' (and it's averted version is 'face01ULI_02.bmp'). 

The image names are code by their file name capital letters ordering (eg. URI, ULC, etc.) This coding is:

Letter Position | Alternative 1                                  | Alternative 2
----------------|------------------------------------------------|-------------------------------------------------------------------
1               | U:  Unfiltered                                 | F: N/A (unused there used to be Filtered versions in the pilot study)
2               | L:  Eye (will be) averted towards the LEFT     | R: Eye (will be) averted towards the RIGHT
3               | I:  The eye gives an Ingongruent cue           | C: The eye gives a Congruent cue 

Lookup table to figure out face stimuli was used based upon cur_tex (direct gaze example):

index | image name
------|-------------
1     | face01URI_01.bmp
2     | face01URC_01.bmp
3     | face01ULI_01.bmp
4     | face01ULC_01.bmp
5     | face02URI_01.bmp
6     | face02URC_01.bmp
7     | face02ULI_01.bmp
8     | face02ULC_01.bmp
9     | face03URI_01.bmp
10    | face03URC_01.bmp
11    | face03ULI_01.bmp
12    | face03ULC_01.bmp
13    | face04URI_01.bmp
14    | face04URC_01.bmp
15    | face04ULI_01.bmp
16    | face04ULC_01.bmp
17    | face05URI_01.bmp
18    | face05URC_01.bmp
19    | face05ULI_01.bmp
20    | face05ULC_01.bmp
21    | face06URI_01.bmp
22    | face06URC_01.bmp
23    | face06ULI_01.bmp
24    | face06ULC_01.bmp
25    | face07URI_01.bmp
26    | face07URC_01.bmp
27    | face07ULI_01.bmp
28    | face07ULC_01.bmp
29    | face08URI_01.bmp
30    | face08URC_01.bmp
31    | face08ULI_01.bmp
32    | face08ULC_01.bmp
33    | face09URI_01.bmp
34    | face09URC_01.bmp
35    | face09ULI_01.bmp
36    | face09ULC_01.bmp
37    | face10URI_01.bmp
38    | face10URC_01.bmp
39    | face10ULI_01.bmp
40    | face10ULC_01.bmp

