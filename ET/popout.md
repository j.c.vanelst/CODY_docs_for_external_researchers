#  Popout
This is a relatively simple free viewing (accompanied by music) experiment. There is no gaze contingency in the actual trials, but there is for the ISI (fixation image), so that the trial is always started with the eye in the center AOI.

## Trial info
At the trial level (saved to disk after each trial ), there are *two* types of .mat files saved to disk. 

 type of trial file | example name (trial 1)
--------------------|--------------------------------------------------------------
gaze data           | B12345_10m_popout_20180115_1703_gazedata_1.mat
onset data          | B12345_10m__20180115_1703_onsets_upto_trial_1.mat

If the task did not crash, the trial info can be found in the aggregated file, that is created after a clean quit or clean finish of the task.

## Aggregated over trial info

The aggregated variant trial randomisation data structure

column | Name            | Description                                | Data shape & type | codings/extra
-------|-----------------|--------------------------------------------|-------------------|------------------
1      | cur_tex         | Index to one of 6 images                   | Double (1-6)      | 1 = POPOUT1.tif, 2 = POPOUT2.tif, etc,
2      | onset           | Timestamp of onset                          | Long integer      | eg. 4.254703753799000e+4
3      | offset          | Timestamp of offset                         | Long integer      | eg. 4.254703753799000e+4

Music and images are both randomised separately, however, the song that was played *cannot* be found in the data.

The fixed mapping for the images:

Number | Image name
-------|---------------
1      | POPOUT1.tif
2      | POPOUT2.tif
3      | POPOUT3.tif
4      | POPOUT4.tif
5      | POPOUT5.tif
6      | POPOUT6.tif

By using cur_tex, one can find out which image was presented, eg. cur_tex values in the order [5 1 3 2 6 4] would imply that order of images has been presented.

