# Opmerkingen/Vragen/Problemen

Hier monitoren we issues (mag in NL) die te maken hebben met het beheren van deze documentatie *in combinatie met* CODY en andere YOUth experiment geralateerde zaken. Wat geen issue meer is, moet weg. 

## gap overlap (e.a.)
In de saccade_trial.m worden bovenstaande infos opgeslagen in ('backup.zip') als trial_x.mat.
In de onsets_up_to_trial_x,mat type files worden de flip_breaks/counts/onsets/etc ook weer apart (dubbel) opgeslagen
In saccade_task.m word al die info weer opgeslagen als geaggregeerde .mat-file. Alleen, als Matlab crasht tussendoor, dan worden de trial-randomisatie info en onset info niet in de geaggregeerde mat files geschreven. Je zou dus kunnen zeggen; als de taak netjes is afgerond, dan mogen die losse 'onsets up to' en trial info files worden weggegooid. Dit moet dan ook in CODY worden aangepast.

 
all_trial_info = [task_type, trial_type, trial_side, gaze_side, trial_img, gazed_cent, gazed_peri, (gazed_cent & gazed_peri), flip_onsets];


## Popout
Caroline had Roy kennelijk gezegd dat de muziek er niet toe deed, het is ook inderdaad niet te achterhalen uit de data. Er zijn 8 nummers waarvan er steeds 6 uit gekozen worden, of zo lijkt het.

## Overal
Check en dubbelcheck alles. Ik ga er 100% van uit dat ik ergens fouten heb gemaakt. Althans, in dit stadium. 
Maar anderzijds is er meer duidelijk dan ooit tevoren, me dunkt.


