# Looking while listening

In this experiment, a sparse visual stimulus with common objects to the left and right is viewed, after some time, a carrier sentence ("Look, a ...", "Where is a ...", "Do you see a...") one of these two objects is addressed . The target speech sound sounds are matched to the asking/stating form. There a two image versions of each pair. Each unique image is presented twice, with targets being either to the left or the right. There are three types of carrier sentences, 

## Data file tags
3Y Looking while listening are tagged *looklisten*  

## Trial info
At the trial level (saved to disk after each trial ), there are *three* types of .mat files saved to disk.

 type of trial file | example name (trial 1)                                       | estimated (median) file size
--------------------|--------------------------------------------------------------|-----------------------------
gaze data           | B12345_3y_looklisten_20180115_1703_gazedata_1.mat            | 300 KB
onset data          | B12345_3y_looklisten_20180115_1703_onsets_upto_trial_1.mat   | < 1 KB (± 600 Bytes)
trial info data     | B12345_3y_looklisten_20180115_1703_trial_1.mat               | < 1 KB (± 600 Bytes)

Similar, aggregated over trials versions of two of these three types can be found if the experiment was completely finished or when a 'clean quit' was used. The onset info is never aggregated as a separate file. We'd strongly recommend to use the trial based file info throughout your analyses, if possible, since they are the real unprocessed 'source' data.

### Detailed trial info

Column  | Name            | Description                       | Data shape & type | codings/extra
--------|-----------------|-----------------------------------|-------------------|------------------
1       | flip_breaks     | There are two 'epochs' in trial. PTB 'flips' the screen multiple times during these epochs, This is logged to be able to check timings etc.. For analysis purposes, none of these 'flip' values are crucial, They are logged to check if PTB behaves as expected. | Usually [0 0], i.e. two values 0 or 1. | Internal logging/verifying purposes of psychophysics toolbox behaviour
2       | flip_counts     | How often the screen has flipped in the different (5) epochs | example [1, 92], | Could be all kinds of values, because some epochs depend on gaze interaction on the fly
3       | flip_onsets     | Timestamps, these are 3 values (T0) | vector with 3 uint64 timestamps with epoch timestamps | It's better to use converted timestamps from gaze data (these are for internal logging/checking purposes)
4       | sent_nr         | Carrier sentence ID (1, 2 or 3) | double (1,2 or 3) | See below for coding
5       | tex_nr          | Index to the (pre-randomised) stimulus struct. In this case the same as trial number | double (between 1-24) |
6       | trial           | Trial number | double (between 1-24) |
7       | vari_nr         | Variant of the image (both types of combo-images have two variants) | double (1 or 2) |
8       | word_nr         | Target word ID (separate coding for the 12 unique words in the experiment) | double (between 1-12) | 

### Carrier sentences coding
integer code | content (english)   | content (dutch) 
-------------|---------------------|--------------------
1            | Look, a ...         | Kijk, een ...
2            | Where is a ...      | Waar is een ...
3            | Do you see a ...    | Zie je een ....

### Left right coding
integer      | meaning 
-------------|-------------
1            | target is left
2            | target is right

### Target word number coding

Number | Dutch     |  English
-------|-----------|-------------  
1      | banaan    | banana 
2      | koekje    | cookie 
3      | bad       | bath 
4      | stoel     | chair
5      | poes      | cat
6      | baby      | baby 
7      | hond      | dog 
8      | koe       | cow       
9      | jas       | jacket 
10     | schoen    | shoe
11     | hand      | hand 
12     | voet      | foot 


The experiment has a fixed order of stimuli presented and items asked about:

trial | image name (.png)     | image ID | carrier sentence | target side | side & target(bold)| Entire sentence
------|-----------------------|----------|------------------|-------------|--------------------|---------------
1     | koekje_banaan1        | 12       | 2                | 2           | cookie **banana**  | Where is a banana?
2     | stoel_bad1            | 11       | 3                | 1           | **chair** bath     | Look, a chair!
3     | baby_poes1            | 10       | 1                | 1           | **baby** cat       | Do you see a baby?
4     | koe_hond1             | 9        | 2                | 2           | cow **dog**        | Where is a dog?
5     | schoen_jas1           | 8        | 1                | 2           | shoe **jacket**    | Look, a jacket!
6     | voet_hand1            | 7        | 3                | 1           | **foot** hand      | Do you see a foot?
7     | banaan_koekje2        | 1        | 1                | 2           | banana **cookie**  | Look, a cookie!
8     | bad_stoel2            | 2        | 3                | 1           | **bath** chair     | Do you see a bath?
9     | poes_baby2            | 3        | 2                | 1           | **cat** baby       | Where is a cat?
10    | hond_koe2             | 4        | 2                | 2           | dog **cow**        | Where is a cow?
11    | jas_schoen2           | 5        | 3                | 2           | jacket **shoe**    | Do you see a shoe?
12    | hand_voet2            | 6        | 1                | 1           | **hand** foot      | Look, a hand!
13    | baby_poes1            | 10       | 3                | 2           | baby **cat**       | Do you see a cat?
14    | koe_hond1             | 9        | 1                | 1           | **cow** dog        | Look, a cow!
15    | bad_stoel2            | 2        | 2                | 2           | **bath** chair     | Where is a bath?
16    | banaan_koekje2        | 1        | 1                | 1           | **banana** cookie  | Look, a banana!
17    | voet_hand1            | 7        | 3                | 2           | foot **hand**      | Do you see a hand?
18    | stoel_bad1            | 11       | 1                | 2           | chair **bath**     | Where is a bath?
19    | schoen_jas1           | 8        | 1                | 1           | **shoe** jacket    | Look, a shoe!
20    | koekje_banaan1        | 12       | 3                | 1           | **cookie** banana  | Do you see a cookie?
21    | hand_voet2            | 6        | 2                | 2           | hand **foot**      | Where is a foot?
22    | hond_koe2             | 4        | 1                | 1           | **dog** cow        | Look, a dog!
23    | poes_baby2            | 3        | 2                | 2           | cat **baby**       | Where is a baby?
24    | jas_schoen2           | 5        | 3                | 1           | **jacket** shoe    | Do you see a jacket?
 

