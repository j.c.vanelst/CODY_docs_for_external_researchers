# Generic descriptions of Eye Tracking Data for YOUth

'Generic', here, implies that the following info holds for any Eye Tracking experiment for the YOUth cohort. Specific info will be a different section.

Original measurement data are somewhere in a folder. TODO: discuss name, make CODY/Yoda behave like specified here.

The folder contains several types of .mat files, which are in fact a Matlab variation of the [HDF5 data organisation standard](https://support.hdfgroup.org/products/hdf5_tools/), This .mat is stored using version 7.3 of the hdf5 functionality in Matlab. 

* These .mat files can be loaded using the Mathworks (TM) Matlab with any version above R2006b (Version 7.3) or later. Older versions may also work, but maybe need some extra flags on loading, see e.g. [this thread](https://nl.mathworks.com/matlabcentral/answers/15521-matlab-function-save-and-v7-3#answer_23983)
* These .mat files can also be loaded/opened using Octave, Python (tested) and R, amongst others.
* These .mat files can however **not** be opened using the regular HDFView software as can be downloaded, eg, from [here](https://www.hdfgroup.org/).

A .mat file contains the following structures, that can be loaded into memory using Matlab with the load function:
 
     load('/some/path/to/A12345_exptag_2023_01_13_all_gazedata.mat')

In the case of this example, a file with Gaze Data, the following variables are then loaded and become visible in the work space:


    all_leftEye:    13 column left eye sample data with numerical values 
    all_rightEye:   13 column right eye sample data with numerical values
    all_timeStamp:  1 column of uint64 (long integer) timestamps in microseconds
    all_trigger:    1 column/row with diode timings (not yet implemented)
    convtimestamp:  1 column/row with uint64 timestamps as converted/synced by PsychToolBox
    trackerID:      1 string that identifies the eye tracker used
    
## Tobii SDK 3.0 documentation
A lot of information about the eye tracker used in this study (Tobii TX-300) can be found in the 2013 version of the Tobii Analytics SDK Developers Guide. A direct link to this document can be found eg [here](https://www.acuity-ets.com/wp-content/uploads/2017/04/Tobii-Analytics-SDK-Developers-Guide.pdf). Note that we need the 8 may 2013 Release 3.0 version, newer versions my have slightly different methods and names, but the general info about coordinate systems etc may still be of help. 

See [tobii](https://Tobii.com) for more info on the newer SDK. We've chosen not to implement newer SDK functions (yet). Due to the nature of this long term project, it has been decided not to adapt to any new functionality, We will however do preliminary tests with the new SDK soon, to assess long term operability. 

Note about timing in the Analytics SDK 3.0 by Jonas Högström (Tobii Pro): "In previous SDK (AnalyticsSDK3.0), we used christians algoritm straight off and I think the interval between updates was something like 1-2 minutes. This could cause quite significant jumps in translated system time. With TX300 I think the changes could exceed the sample2sample interval of 3.3ms causing the gaze data timestamps to appear to be in the incorrect order if there was a significant drift between the clocks."
 
## Gaze data
There are files with onset timing info (they contain an 'onsets_upto_trial_x' pattern in their name). There are also files that simply have the pattern "trial_x" (with x being the trial number). They contain coding of specific experiment-bound values like image ID, trial randomisation info (eg. was the target left or right? What was the stimulus at trial X? etc.). We will discuss that kind of information in the specific descriptions for each experiment. Here, we describe the generic part; *gaze data* (at the trial level).
  
Left gaze data and right gaze data are of *identical* structure, 13 columns, all of type 'double'. Usually, Researchers want to use GazePoint2d.x and GazePoint3d.y (columns 7 and 8 )as the signals for x and y, but beware that Validity is also important: value's with validity codes any other than 0, may be unreliable and are perhaps best treated as missing data (Not A Number). Codes can mean the following:

## Validity codes
    0  Both left and right eye were correctly found (found two eyes)  
    1  Either the left or the right eye was found (depends on data left or right which one was found)   
    2  One eye was found, but unsure which one  
    3  Either the left or the right eye was found (depends on data left or right which one was found)   
    4  No eyes were found  
    

## Gaze data format (identical for left or right eye)

id | Column name (not loaded)| Description                                                                                               | Unit                             
---|-------------------------|-----------------------------------------------------------------------------------------------------------|------------------------ 
1  | EyePosition3d x         | X coordinate of the eye position in 3D space given in the User Coordinate System.                         | cm                          
2  | EyePosition3d y         | Y coordinate of the eye position in 3D space given in the User Coordinate System.                         | cm                             
3  | EyePosition3d z         | Z coordinate of the eye position in 3D space given in the User Coordinate system,                         | cm                          
4  | EyePosition3dRelative x | X position of the left eye in 3d space relative to the track box given in the Track Box Coordinate System | prop, of display (0-1)
5  | EyePosition3dRelative y | Y position of the left eye in 3d space relative to the track box given in the Track Box Coordinate System | prop. of display (0-1) 
6  | EyePosition3dRelative z | Z position of the left eye in 3d space relative to the track box given in the Track Box Coordinate System | prop. of display (0-1)   
7  | GazePoint2d x           | X coordinate of the gaze point on the calibration plane given in the Active Display Coordinate System.    | prop. of display (0-1)   
8  | GazePoint2d y           | Y coordinate of the gaze point on the calibration plane given in the Active Display Coordinate System.    | prop. of display (0-1)  
9  | GazePoint3d x           | X coordinate of the gaze point on the calibration plane given in the User Coordinate System.              | prop. of display (0-1)  
10 | GazePoint3d y           | Y coordinate of the gaze point on the calibration plane given in the User Coordinate System.              | prop. of display (0-1)  
11 | GazePoint3d z           | Z coordinate of the gaze point on the calibration plane given in the User Coordinate System.              | prop. of display (0-1) 
12 | PupilDiameter           | Diameter of the pupil in millimeters.                                                                     | millimeters                
13 | Validity                | Validity code (0-4) for the eye.                                                                          | 0, 1, 2, 3 or 4
 
## Gaze timeStamps as reported by Tobii (all_timeStamp)

Contains a single row/column of data in Long integer format (microseconds), starting at some (arbitrary?) number. This data contains just as much data points as the gaze data has samples, eg. :

sample | Value
-------|----------            
1      | 1509725018066252 
2      | 1509725018069574
3      | 1509725018072907

The difference between the first two samples is 3322 microseconds, the second exactly 3333, which is indeed approximately the sample rate 1/300 --> 0.00333 seconds, 3.3333 milliseconds. You can normalise the time signal in time (onset) by subtracting the first timestamp value from all the others, so it could be used in trials like such:

sample | Value
-------|----------            
1      | 0    (0 µs)
2      | 3322 (3222 µs)
3      | 6655 (3333 µs)

## Trigger timestamps
These are not implemented, but may be in the future. They could contain trial/screen transitions based upon measurements made by a photodiode of light (white) and dark (black) blocks as measured on screen (top left), 

## Converted timestamps
These are the timestamps as converted by the tetio (eye tracking functions by Tobii SDK) function called tetio_remoteToLocalTime. It converts Eye Tracker based timestamps as eye to computer time.

## Calibration data file names and structure
Calibration mat-files contain the following info:  
  
* X and Y positions, given in proportion of the display used (values always between 0-1)  
* (A selection of) left eye samples for X and Y positions as measured by the eye tracker (0-1)  
* (A selection of) right eye samples for X and Y positions as measured by the eye tracker (0-1)  

More specifically, the mat-file with calibration info (saved as variable ‘pts') is an 8 (columns) by variable ('samples') vector with sample's as gathered during a manually paced calibration routine with the following format:


column  |  Column values                                                   | Description
--------|------------------------------------------------------------------|-------------            
1       | X coordinates of administered calibration point                  | Value between (0-1) of the X screen dimension
2       | Y coordinates of administered calibration point                  | Value between (0-1) of the Y screen dimension
3       | X coordinates of 'best fit' measured data at point for LEFT eye  | Value between (0-1) of the X screen dimension
4       | Y coordinates of 'best fit' measured data at point for LEFT eye  | Value between (0-1) of the X screen dimension
5       | Validity code for measured data at point for LEFT eye            | 0, 1, 2, 3 or 4 (see Tobii SDK for captions)
6       | X coordinates of 'best fit' measured data at point for RIGHT eye | Value between (0-1) of the X screen dimension
7       | Y coordinates of 'best fit' measured data at point for RIGHT eye | Value between (0-1) of the X screen dimension
8       | Validity code for measured data at point for RIGHT eye           | 0, 1, 2, 3 or 4 (see Tobii SDK for captions)


* Each point for which a calibration point + data has been accepted by the operator ends up in this file. If at some point no valid data has been accepted, the data is not included.
* During the calibration routine, five points are administered. At each point, when the operator hits the space bar during calibration, a small amount of samples from the eye tracker is collected. Read more about the details of the calibration procedure and stimuli [here](http://onlinelibrary.wiley.com/doi/10.1111/infa.12093/abstract).
* The same type of experimenter-paced 5-point, "contracting spiral" calibration stimuli are used throughout age groups (i.e. "waves").
* Sometimes, files do not contain a file (calib_X.mat). In this case, a previous calibration has been re-used, usually because it has been too difficult or time-consuming to obtain reasonable results while calibrating. In most of the experiments conducted for YOUth, the stimuli are sparse (only three locations, relatively far from each other given the screen), which has the benefit that accuracy is not likely to be the bottleneck for data analysis.
* For data collected prior to December 2017, there has been no failsafe/unique way to identify which calibration belongs to which experiment. Files were name "Pseudocode_Wave_Experiment_ExpStartupTime>_calib_X.m", where X is the number of calibrations preformed for that experiment. This way, it is not 100% clear which calibration applies, given the gaze data. 
* As of december 2017 and onwards, each calibration file has the following naming convention:

The calibration for each task now has the naming convention:

    Pseudocode_Wave_Experiment_ExperimentStartupTime_calib _ISODatetimestampAtCalibTime_ExpCalibCount_trial_0.m
    |          |    |          |                     |      |                           |             |     |
    Code       Wave Experiment Exp. startup time     ...    ISO datetime real time      # Calibs      ...   at trial        


Any recalibration after a pause, repositioning then gets it's own ISO timestamp and trial number (X value), 

eg: 

    A12345_9y_chprogap_20171103_1634_calib_20171103T163509_1_trial_0.mat (first calibration for experiment chprogap)
    A12345_9y_chprogap_20171103_1634_calib_20171103T163544_2_trial_2.mat (second calibration done just before trial 2)
    A12345_9y_chprogap_20171103_1634_calib_20171103T163612_3_trial_5.mat (third calibration done just before trial 5)

