# Gap-Overlap experiments
Many waves (will) have some implementation of the saccade/anti-saccade task, combined with the Gap-Overlap paradigm. Infants cannot yet be instructed to make an anti-saccade, so this version of the task is not administered until children are around ± 9 years old. These type of experiments are all gaze-contingent, i.e. the peripheral stimulus does not appear until the center one has been gazed by the eye for at least 20 milliseconds. Also, a "reward" frame of spinning/looming can be triggered as soon as 20 ms of eye samples have been found in the right or left (and, correct) Area Of Interest. However, this animation is also started 1500 milliseconds after the peripheral stimulus has been presented. 

## Data file tags
5 and 10 month gap overlap experiments are tagged *infprogap*  
9 year gap overlap prosaccade experiments are tagged *chprogap*  
9 year gap overlap antisaccade experiments are tagged *chantigap*  

## Conditions

Coding in data: 1=GAP, 2=OVERLAP, 3=BASELINE

**'Gap'** implies that there is a time gap of 200 milliseconds between the disappearance of the central stimulus and the appearance of the peripheral stimulus.
**'Overlap'** implies that the central and peripheral stimulus are *both* visible for 200 milliseconds before the central stimulus disappears.
**'Baseline'** implies that the central disappears exactly on the same time that the peripheral one appears.

## Trial info
At the trial level (saved to disk after each trial), There are *three* types of .mat files saved to disk.

type of trial file  | example name (trial 1)                                     | estimated (median) file size
--------------------|------------------------------------------------------------|------------------------------
gaze data           | B12345_5m_infprogap_20180115_1703_gazedata_1.mat           |  200 KB
onset data          | B12345_5m_infprogap_20180115_1703_onsets_upto_trial_1.mat  |  < 1 KB (± 800 bytes)
trial info data     | B12345_5m_infprogap_20180115_1703_trial_1.mat              |  < 1 KB (± 700 bytes)

Similar, aggregated over trials versions of two of these three types can be found if the experiment was completely finished or when a 'clean quit' was used. The onset info is never aggregated as a separate file. We'd strongly recommend to use the trial based file info throughout your analyses, if possible, since they are the real unprocessed 'source' data.

There are 48 or 60 trials, depending on a crude estimation of data quality, as determined by the stimulus presentation program on the fly.

### Detailed trial info 

The pro and anti gap trial files contain:

Name            | Description   | Data shape & type | codings/extra
----------------|------------------------------------------------------------------------------------------------|-------------------|------------------
all_gazed       | Indicates wether the eye has hit the (gaze contingent AOI's) for center and peripheral stimulus. If at least six data samples have been within an AOI, values become 1, so if both stimuli have been 'touched' by the eye, both values are 1. | Boolean tuple, possible combinations: [1 1], [0 0], [1 0] or [0 1] | The first place is for center stimulus, the second for peripheral 
flip_breaks     | There are five 'epochs' in a progap experiment trial. PTB 'flips' the screen multiple times during these epochs (with different specifications for looming and throbbing, presenting different element), This is logged to be able to check timings etc.. For analysis purposes, none of these 'flip' values are crucial, they are logged to check if PTB behaves as expected. | Usually [0 0 0 0 0], i.e. five values 0 or 1. | Internal logging/verifying purposes of psychophysics toolbox behaviour
flip_counts     | How often the screen has flipped in the different (5) epochs | example [1, 40, 12, 24, 60], | Could be all kinds of values, because some epochs depend on gaze interaction on the fly
flip_onsets     | Timestamps, these are six values (T0) | vector with 6 uint64 timestamps with epoch timestamps | It's better to use converted timestamps from gaze data (these are for internal logging/checking purposes)
task_type       | Experiment specification (internal check) | Double 2, 3 or 4 | Coding: 4:Around 0 and 3 years (only pro saccade), 3:Anti saccade, 2:Pro Saccade |
trial_img     | Once, we used different types of peripheral target stimuli, 3 is now the only one used (default) | Double (only value 3) | Other values than 3 have been deprecated 
trial_side      | Indicates on which side the target was presented | Double (1 or 2) | 1: right side, 2: left side (IMPORTANT: Jacco originaly mentioned vice versa, Mark confirms 1 right, 2 left. Could this vary over time?)
trial_type      | Indicates gap, overlap or baseline | Double (1, 2 or 3) | 1:Gap, 2:Overlap, 3:Baseline 

The same variable names apply for the '(*)all_trials.mat' aggregated versions. 

