# Specific descriptions of Eye Tracking data for YOUth

## Current status of waves and experiments at each wave
At the data storage level, in order to make sure no mistakes will be made, the experiment names as used in the task administration are often different, even though the experiments are of the same type, As an example: the infant (5 or 10 month old children) version of the 'saccade gap overlap task' is called 'infprogap', whereas the equivalent version administered to children around the age of 9 is called "chprogap" (child pro gap). It is not yet known for planned waves if there will be other naming conventions. The two versions that are currently running production (child vs infant) do have different parameters for stimulus presentation, number of trials, etc, 

The following waves are planned, Waves that are already running production at the moment of this writing are in **bold**.  

* **Around 0 years** (first year)
    * **5m**  (around 5 months)
    * **10m** (around 10 months)
* Around 3 years (pilots already conducted, nearing start of wave at the moment of writing)
* Around 6 years
* **Around 9 years**
* Around 12 years
* Around 15 years

The current names and experiments are already (fully) specified, although actual production measurements for Around 3 have not yet started.  

Wave | Verbose name                    | Data file name |  Comments
-----|---------------------------------|----------------|----------------------------------------
5m   | Infant pro gap overlap          | infprogap      | Gap Overlap paradigm pro saccade task.                    
5m   | Infant Social gaze              | infsgaze       | Cueing task in which the effect of a switch from direct to averted (left or right) gaze is investigated
5m   | Face popout                     | popout         | Free viewing task in which different types of objects like faces, electronic devices, cars, animals and random dot face-like images are viewed. Viewing preferences are of interest.     
10m  | Infant pro gap overlap          | infprogap      | (Same as 5m)                   
10m  | Infant Social gaze              | infsgaze       | (Same as 5m)
10m  | Face popout                     | popout         | (Same as 5m)
3y   | Infant pro gap overlap          | infprogap      | (Same as 5m & 10m)                    
3y   | Infant Social gaze              | infsgaze       | (Same as 5m & 10m)
3y   | Face popout                     | popout         | (Same as 5m & 10m)
3y   | Looking while listening         | looklisten     | Audio-driven 2 alternatives based viewing task ("Do you see a cookie?" While eg aa cookie and a banana are presented tot the left or right side of the screen.) 
9y   | Child pro gap overlap           | chprogap       | Same as the before mentioned 5m/10m/3y experiments, although presentation times are shorter, also the number of stimuli)
9y   | Child anti gap overlap          | chantigap      | Antisaccade variation of the gap overlap experiment. Children are asked to make a saccade to the opposite direction of where the peripheral stimulus appears.
9y   | Child social gaze               | chsgaze        | Social gaze task like the 5m/10m/3y equivalents, only stimulus presentations are shorter

