# Eye-tracking data collection and analysis

This document contains a justification of the choices that were made in the eye-tracking data collection and analysis of the YOUth cohort study. For information, contact **Roy Hessels**, *royhessels@gmail.com*.

## Choices and justification

### Eye-tracking data collection
Tobii TX300 eye tracker is used across all age ranges. Most robust to head movement, based on: 

	Hessels, R. S., Cornelissen, T. H. W., Kemner, C., & Hooge, I. T. C. (2015). Qualitative tests of remote eyetracker recovery and performance during head rotation, 47(3), 848–859. http://doi.org/10.3758/s13428-014-0507-6*
	Niehorster, D. C., Cornelissen, T. H. W., Holmqvist, K., Hooge, I. T. C., & Hessels, R. S. (2017). What to expect from your remote eye-tracker when participants are unrestrained. Behavior Research Methods, 1–15. http://doi.org/10.3758/s13428-017-0863-0

The calibration procedure is operator-controlled. This has been shown to result in higher eye-tracking data quality than system-controlled calibration. *note*: Participant-controlled calibration yields even higher data quality, but is not possible with infants. This choise is based on:

	Nyström, M., Andersson, R., Holmqvist, K., & van de Weijer, J. (2013). The influence of calibration method and eye physiology on eyetracking data quality, 45(1), 272–288. http://doi.org/10.3758/s13428-012-0247-4

### Positioning of participant and eye tracker
Positioning of Rondom 0. Preferably in baby/car seat fixed to table. If not possible, switch to high chair or child on parents' lap. Based on:
	
	Hessels, R. S., Andersson, R., Hooge, I. T. C., Nyström, M., & Kemner, C. (2015). Consequences of Eye Color, Positioning, and Head Movement for Eye-Tracking Data Quality in Infant Research. Infancy, 20(6), 601–633. http://doi.org/10.1111/infa.12093

### Eye-tracking data analysis
Validity codes in the eye-tracking data above 1 are removed from the analysis. Based on the Tobii SDK manual and:

	Niehorster, D. C., Cornelissen, T. H. W., Holmqvist, K., Hooge, I. T. C., & Hessels, R. S. (2017). What to expect from your remote eye-tracker when participants are unrestrained. Behavior Research Methods, 1–15. http://doi.org/10.3758/s13428-017-0863-0

Identification by 2-means clustering fixation-classification algorithm is used for all age ranges. It has been shown to be most robust to noise in:

	Hessels, R. S., Niehorster, D. C., Kemner, C., & Hooge, I. T. C. (2017). Noise-robust fixation detection in eye movement data: Identification by two-means clustering (I2MC). Behavior Research Methods, 49(5), 1802–1823. http://doi.org/10.3758/s13428-016-0822-1
	 
Moreover, it has already been succesfully applied to gap-overlap data in infant and adolescent data. See: 

	Cousijn, J., Hessels, R. S., Van der Stigchel, S., & Kemner, C. (2017). Evaluation of the Psychometric Properties of the Gap‐Overlap Task in 10‐Month‐Old Infants. Infancy, 1–9. http://doi.org/10.1111/infa.12185
	Van der Stigchel, S., Hessels, R. S., van Elst, J. C., & Kemner, C. (2017). The disengagement of visual attention in the gap paradigm across adolescence. Experimental Brain Research, 1–8. http://doi.org/10.1007/s00221-017-5085-2*



